const mongoose = require('mongoose');

const categoriaSchema = mongoose.Schema({
    codigo: Number,
    nome: String,
});

module.exports = mongoose.model('categoria', categoriaSchema);