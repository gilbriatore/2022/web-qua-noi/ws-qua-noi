require('./db/mongodb');
const produtoRouter = require('./routes/produtoRouter');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const servidor = express();
servidor.use(morgan('dev'));
servidor.use(express.urlencoded({'extended': true}));
servidor.use(express.json());
servidor.use(cors());

servidor.use('/produtos', produtoRouter);

servidor.get('/', (req, res) => {
    res.send('Servidor Express (GET)...');
});

servidor.listen(3000, ()=> {
    console.log('Servidor http://localhost:3000...');
});