const produtoModel = require('../models/produtoModel');

class ProdutoController {

    async salvar(req, res) {
        const produto = req.body;

        //Implementação para gerar novo código.
        const max = await produtoModel.findOne({}).sort({'codigo': -1});
        produto.codigo = max == null ? 1 : max.codigo + 1;

        //Implementação para salvar o produto.
        const resultado = await produtoModel.create(produto);
        res.status(201).json(resultado);
    }

    async buscarPorCodigo(req, res) {
        const codigo = req.params.codigo;
        const produto = await produtoModel.findOne({'codigo': codigo});        
        res.status(200).json(produto);
    }

    async listar(req, res) {
        const produtos = await produtoModel.find({});
        res.json(produtos);
    }

    async atualizar(req, res) {
        const codigo = req.params.codigo;
        const produto = req.body;
        await produtoModel.findOneAndUpdate({'codigo': codigo}, produto);        
        res.status(200).send();        
    }

    async apagar(req, res) {
        const codigo = req.params.codigo;
        await produtoModel.findOneAndDelete({'codigo': codigo});        
        res.status(200).send();
    }

}

module.exports = new ProdutoController();