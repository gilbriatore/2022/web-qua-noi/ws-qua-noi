import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Produto } from 'src/models/produto.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadastroService {

  constructor(private http: HttpClient) { }

  URL = "http://localhost:3000/produtos";

  listarProdutos() : Observable<Produto[]>{    
    return this.http.get<Produto[]>(this.URL);
  }

  buscarProdutoPorCodigo(codigo : number) : Observable<Produto>{
    return this.http.get<Produto>(this.URL + "/" + codigo);
  }

  incluirProduto(produto : Produto) : Observable<any> {
    return this.http.post(this.URL, produto);
  }

  atualizarProduto(produto: Produto, codigo : number) : Observable<any> {
    return this.http.put(this.URL + "/" + codigo, produto);
  }

  excluirProduto(codigo : number) : Observable<any> {
    return this.http.delete(this.URL + "/" + codigo);
  }
}
